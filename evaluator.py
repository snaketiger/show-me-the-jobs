#
# Copyright 2023 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import re
from configparser import SectionProxy
from jobscraper import Job


class Evaluator:
    @classmethod
    def init_patterns(cls, config: SectionProxy):
        flags = 0
        if "IGNORECASE" in config and config["IGNORECASE"] == "True":
            flags = re.I
        patterns = []
        for key in config:
            if key.startswith("re"):
                try:
                    patterns.append(re.compile(config[key], flags))
                except re.error as e:
                    raise ValueError(f"Could not compile {key} from {config.name}: '{config[key]}'") from e
        return patterns

    def __init__(self, title_cfg: SectionProxy, desc_cfg: SectionProxy):
        self.title_re = self.init_patterns(title_cfg)
        self.title_exec = [title_cfg[key] for key in title_cfg if key.startswith("exec")]
        self.description_re = self.init_patterns(desc_cfg)
        self.description_exec = [desc_cfg[key] for key in desc_cfg if key.startswith("exec")]

    def exec_with_output(self, code: str, job: Job, score: int):
        output = locals()
        exec(code, {}, output)
        return output["score"]

    def eval(self, job: Job):
        score = 0
        for code in self.title_exec:
            score = self.exec_with_output(code, job, score)
        for code in self.description_exec:
            score = self.exec_with_output(code, job, score)
        return score
