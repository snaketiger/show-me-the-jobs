# show-me-the-jobs

Description
========================================
This job scraping tool is designed to provide extensible support for multiple websites.

Installation
========================================
1. Navigate a cmd to this directory.
2. Input "python setup.py install"

Configuring user.cfg
========================================
A variety of customizable values can be found in user.cfg
1. scraper
   - keyword: What job are you seeking?
   - user_agent: Provides the User-Agent string for the web requests.

        `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36`

   - where: Where are you interested in working?
2. eval.job.title
   - IGNORECASE: When True, the regex patterns in this section use case-insensitive matching.
   - re#: Regex patterns that the exec strings use to score each job's title. [^1]
   - exec#: Python code strings that perform the scoring for the job titles.
3. eval.job.description
   - IGNORECASE: When True, the regex patterns in this section use case-insensitive matching.
   - re#: Regex patterns that the exec strings use to score each job's description. [^1]
   - exec#: Python code strings that perform the scoring for the job descriptions.
4. sort_jobs
   - city_pref_desc: A comma-separated list of cities in descending order of preference.

**Some exec# Examples**
1. `score -= 2 if ${TL}0${TR} else (1 if ${TL}1${TR} else 0)`  
   Deduct 2 if title matches re0, 1 if title matches re1, 0 otherwise
2. `score += 1 if ${DL}1${DR} and ${DL}2${DR} else 0`  
   Add 1 if description matches re1 and re2, 0 otherwise

Usage
========================================
```
show-me-the-jobs.py [-h] [-m MAX] [-q] {OregonJobs,SimplyHired}

Scrapes one of the supported sites for jobs and writes them to jobs.csv.

positional arguments:
  {OregonJobs,SimplyHired}
                        choose a job Scraper

optional arguments:
  -h, --help            show this help message and exit
  -m MAX, --max MAX     when jobs scraped equals MAX, stop
  -q, --quiet           suppress Scraper messages

job Scrapers:
  OregonJobs (secure.emp.state.or.us)
  SimplyHired (www.simplyhired.com)
```

How to scrape a site that's unsupported
========================================
1. Copy [skeleton.py](jobscraper/skeleton.py)
2. Start by filling in the FIXME lines and adding site specifics
3. Add an import line for the new file to [\_\_init\_\_.py](jobscraper/__init__.py)

Authors and acknowledgment
========================================
@pso-eng

License
========================================
[LICENSE](LICENSE)

[^1]: https://docs.python.org/3/library/re.html#regular-expression-syntax