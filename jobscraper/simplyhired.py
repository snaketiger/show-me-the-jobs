#
# Copyright 2023 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from bs4 import BeautifulSoup
from configparser import SectionProxy
from datetime import date, timedelta
import html as HTML
import re
from urllib.error import HTTPError
from .scraper import Job, Scraper


class SHJob(Job):
    """This class stores data extracted by the SimplyHired Scraper."""

    def __init__(self, title):
        super().__init__()
        self.title = title
        self.company = ''

    def as_csv(self) -> str:
        posted = f"{self.date.month}/{self.date.day}/{self.date.year}"
        return f'"{self.title}","{self.company}",{self.location},{posted},"{self.salary}",{self.score},{self.url}'

    def csv_header(self) -> str:
        return "Title,Company,Location,Date Posted,Salary,Score,URL"


class SimplyHired(Scraper):
    """This class scrapes listings from SimplyHired's job search."""
    domain = "www.simplyhired.com"

    @property
    def jobs_per_page(self) -> int:
        return 0

    @property
    def page(self) -> int:
        return self._page

    def __init__(self, config: SectionProxy, quiet: bool = False, stop=None):
        """Instantiate a SimplyHired Scraper.
        :param config: Configuration details (must include keyword and user_agent)
        :param quiet: When True (default: False), Scraper messages are suppressed.
        :param stop: When >= 1 (default: None), ends scraping once {stop} jobs are returned.
        """
        super().__init__(
            config["keyword"], '" href="/job/(?P<id>[0-9A-Za-z-_]{54})', config["user_agent"], quiet, stop
        )
        url_l = config["where"].replace(',', "%2C").replace(' ', '+')
        self.base = f"https://{self.domain}/"
        self.query = f"{self.base}search?q={self.job_kw}&l={url_l}&jt=CF3CP&pn=1"
        self._next_page = ''
        self._page = 1

    def turn_page(self, html: str) -> None:
        self.print(f"On page {self.page}")
        self.id_iter = self.id_re.finditer(html)
        if self.x < self.hits - 10:
            cursor_re = re.compile(f'" href=".*cursor=(?P<cursor>[0-9A-Za-z%]+)">{self.page + 1}</a>')
            self._next_page = f'&cursor={cursor_re.search(html).group("cursor")}'
        self._page += 1

    def next_id(self) -> str:
        try:
            match = self.next_id_match()
        except StopIteration:
            html = self.url_open_read_decode(self.query + f"{self._next_page}")
            self.turn_page(html)
            match = self.next_id_match()
        return match.group("id")

    def __next__(self):
        self.start_next()
        if self.x == 1:
            try:
                html = self.url_open_read_decode(self.query + f"{self._next_page}")
                self.init_results(html, r'<p class="chakra-text css-\w+">(?P<hits>\d+)</p>')
                self.turn_page(html)
            except Exception:
                raise StopIteration
        try:
            jid = self.next_id()
        except HTTPError:
            raise StopIteration
        url = f"{self.base}job/{jid}?isp=0&q={self.job_kw}"
        if jid in self.ids:
            self.print(f"Skipping duplicate ({url})")
            return self.__next__()
        else:
            self.ids.append(jid)
            try:
                page = BeautifulSoup(self.url_open_read_decode(url), "html.parser")
                title = HTML.unescape(page.find("title").text)
                job = SHJob(title.split(" - ")[0])
                job.company = (title.split(" - ")[-1]).split(" | ")[0]
                location = title.split(" | ")[1][:-4]
                if location not in ["Or", "United St"]:
                    job.location = location
                age = page.select("[data-testid='VJ-jobDetails-viewJobAge']")
                today = date.today()
                if len(age) > 0 and "days ago" in age[0].text:
                    when = today - timedelta(int(age[0].text.split(' ')[0]))
                    job.date = when
                else:
                    job.date = today
                comp = page.select('[data-testid="viewJobBodyJobCompensation"]')
                if len(comp) > 0:
                    job.salary = comp[0].text[1:].replace("Estimated: ", '')
                job.url = url
                for did in ["VJ-section-content-jobDescription", "viewJobBodyJobFullDescriptionContent"]:
                    result = page.select(f"[data-testid='{did}']")
                    if len(result) > 0:
                        job.description = HTML.unescape(str(result[0]))
                        break
                return job
            except HTTPError:
                return self.__next__()

    @classmethod
    def describe(cls) -> str:
        return f"SimplyHired ({cls.domain})"
