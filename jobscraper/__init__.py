from .scraper import Job, Scraper
from .oregonjobs import OregonJob, OregonJobs
from .simplyhired import SHJob, SimplyHired