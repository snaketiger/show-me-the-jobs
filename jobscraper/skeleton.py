#
# Copyright 2023 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from bs4 import BeautifulSoup
from configparser import SectionProxy
from datetime import datetime
import re
from urllib.error import HTTPError
from .scraper import Job, Scraper


class JobSkeleton(Job):
    """This class stores data extracted by the FIXME"""

    def __init__(self, title):
        super().__init__()
        self.title = title

    def as_csv(self) -> str:
        posted = f"{self.date.month}/{self.date.day}/{self.date.year}"
        return f'"{self.title}",{self.location},{posted},"{self.salary}",{self.score},{self.url}'

    def csv_header(self) -> str:
        return "Title,Location,Date Posted,Salary,Score,URL"


class ScraperSkeleton(Scraper):
    """This class scrapes listings from FIXME"""
    domain = "www.notawebsite.com"  # FIXME

    @property
    def jobs_per_page(self) -> int:
        return 10

    def __init__(self, config: SectionProxy, quiet: bool = False, stop=None):
        """Instantiate a FIXME
        :param config: Configuration details (must include keyword and user_agent)
        :param quiet: When True (default: False), Scraper messages are suppressed.
        :param stop: When >= 1 (default: None), ends scraping once {stop} jobs are returned.
        """
        super().__init__(config["keyword"], r"id:(?P<id>\w+)", config["user_agent"], quiet, stop)  # FIXME: pattern str
        self.base = f"https://{self.domain}/jobs/"  # FIXME
        self.query = f"search?{self.base}keyword={self.job_kw}&loc={config['where']}"  # FIXME

    def __next__(self):
        self.start_next()
        if self.x % self.jobs_per_page == 1:
            try:
                html = self.url_open_read_decode(self.query + str(self.x))
                if self.x == 1:
                    self.init_results(html, r"hits:(?P<hits>\d+)")  # FIXME: pattern str
                self.turn_page(html)
            except Exception:
                raise StopIteration
        jid = self.next_id_match().group("id")
        url = f"{self.base}{jid}/"  # FIXME
        if jid in self.ids:
            self.print(f"Skipping duplicate ({url})")
            return self.__next__()
        else:
            self.ids.append(jid)
            try:
                page = BeautifulSoup(self.url_open_read_decode(url), "html.parser")
                tds = page.find_all("td")
                spans = page.find_all("span")
                job = JobSkeleton("title goes here")  # FIXME
                job.location = "location goes here"  # FIXME
                job.date = datetime(1111, 22, 33)  # FIXME: datetime(year, month, day)
                job.url = url
                job.description = "description goes here"  # FIXME
                job.salary = "salary goes here"  # FIXME
                return job
            except HTTPError:
                return self.__next__()

    @classmethod
    def describe(cls) -> str:
        return f"Foobar ({cls.domain})"  # FIXME
