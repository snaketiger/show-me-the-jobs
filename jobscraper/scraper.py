#
# Copyright 2023 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from abc import ABCMeta, abstractmethod
import math
import re
from urllib.error import HTTPError
from urllib.request import Request, urlopen


class Job(metaclass=ABCMeta):
    """This class stores data extracted from a job listing.

    The as_csv and csv_header methods are not implemented by this class.
    """

    def __init__(self):
        self.title = ''
        self.location = ''
        self.date = None
        self.description = ''
        self.salary = ''
        self.score = None
        self.url = ''

    @abstractmethod
    def as_csv(self) -> str:
        """Return the job data as a CSV row."""
        raise NotImplementedError

    @abstractmethod
    def csv_header(self) -> str:
        """Return column headers to go with the CSV row(s)."""
        raise NotImplementedError


class Scraper(metaclass=ABCMeta):
    """A scraper is an iterator.

    This class implements all its methods except for describe, jobs_per_page and __next__.
    """

    def __init__(self, job_kw: str, pattern: str, user_agent: str, quiet: bool = False, stop = None):
        """Instantiate a Scraper.
        :param job_kw: Provides the keyword string for the search query.
        :param pattern: A regex that matches each individual job id.
        :param user_agent: Provides the User-Agent string for any urllib Request.
        :param quiet: When True (default: False), Scraper messages are suppressed.
        :param stop: When >= 1 (default: None), ends scraping once {stop} jobs are returned.
        """
        self.hits = None
        self.id_iter = None
        try:
            self.id_re = re.compile(pattern)
        except re.error as e:
            raise ValueError(f"Could not compile pattern: '{pattern}'") from e
        self.ids = []
        self.job_kw = job_kw.replace(' ', '+')
        self._pages = None
        self._quiet = quiet
        self._range = None
        if stop is not None and stop < 1:
            self._stop = None
        else:
            self._stop = stop
        self._user_agent = {
            "User-Agent": user_agent
        }
        self.x = 1

    @property
    @abstractmethod
    def jobs_per_page(self) -> int:
        """Return the number of jobs on each page."""
        raise NotImplementedError

    @property
    def page(self) -> int:
        """Return the current page number."""
        add_one = (self.x - 1) // self.jobs_per_page
        return add_one + 1

    def __iter__(self):
        return self

    @abstractmethod
    def __next__(self):
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def describe(cls) -> str:
        """Return a description for the Scraper subclass."""
        raise NotImplementedError

    def start_next(self):
        """Increment self.x, and perform the stop check."""
        if self._range is not None:
            self.x = self._range.__next__()
        if self._stop and len(self.ids) == self._stop:
            raise StopIteration

    def url_open_read_decode(self, url: str) -> str:
        """Open {url}, read the response body, and decode its bytes.

        :return: The utf-8 decoded body.
        """
        req = Request(url, headers=self._user_agent)
        try:
            with urlopen(req) as r:
                return r.read().decode("utf-8")
        except HTTPError as e:
            print(f"HTTPError: {e.code} ({e.reason})")
            print(f"Error.url: {url}")
            raise

    def print(self, text: str) -> None:
        """Prints {text} to stdout, but only if self._quiet is False."""
        if not self._quiet:
            print(text)

    def init_results(self, html: str, pattern: str):
        """Determine count by searching {html} for {pattern}.
        (Count used to initialize result range)
        """
        hits_re = re.compile(pattern, re.I)
        self.hits = int(hits_re.search(html).group("hits"))
        if self.hits == 0:
            self.print("Your search results are void")
            raise StopIteration
        self.print(f"Your search results include {self.hits} jobs")
        self._range = range(2, self.hits + 1).__iter__()

    def turn_page(self, html: str) -> None:
        """Fill the iterator with job id matches found in {html}."""
        if self._pages is None:
            self._pages = math.ceil(self.hits / self.jobs_per_page)
        right = self.hits if self.page == self._pages else (self.x + self.jobs_per_page - 1)
        self.print(f"On page {self.page}/{self._pages} (jobs {self.x}-{right})")
        self.id_iter = self.id_re.finditer(html)

    def next_id_match(self):
        """Return the next job id match."""
        return self.id_iter.__next__()
