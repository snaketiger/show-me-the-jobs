#
# Copyright 2023 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from bs4 import BeautifulSoup
from configparser import SectionProxy
from datetime import datetime
import re
from urllib.error import HTTPError
from .scraper import Job, Scraper

str_to_loc = {
    "-- State Wide --": "000",
    "Oregon": "000",
    "Albany": "008",
    "Ashland": "038",
    "Astoria": "019",
    "Baker City": "030",
    "Beaverton": "033",
    "Bend": "023",
    "Brookings": "010",
    "Burns": "031",
    "Central Oregon": "011%2C012%2C020%2C023%2C024%2C043%2C044%2C048",
    "Coos Bay": "015",
    "Corvallis": "039",
    "Dallas": "040",
    "Portland-Downtown": "032",
    "Eastern Oregon": "022%2C025%2C026%2C027%2C028%2C029%2C030%2C031%2C041",
    "Enterprise": "041",
    "Eugene/Springfield": "018%2C050",
    "Florence": "042",
    "Gold Beach": "009",
    "Grants Pass": "016",
    "Gresham": "003",
    "Hermiston": "022",
    "Hillsboro": "002",
    "Hood River": "043",
    "John Day": "025",
    "Klamath Falls": "024",
    "La Grande": "028",
    "Lakeview": "044",
    "Lebanon": "045",
    "Lincoln City": "046",
    "Madras": "012",
    "McMinnville": "004",
    "Medford": "021",
    "Milton-Freewater": "027",
    "Newberg": "047",
    "Newport": "014",
    "Portland-North": "037",
    "Ontario": "029",
    "Oregon City": "005",
    "Oregon Coast": "009%2C010%2C013%2C014%2C015%2C019%2C046%2C042",
    "Pendleton": "026",
    "Portland Area": "002%2C003%2C004%2C005%2C006%2C032%2C033%2C037%2C047%2C051%2C194%2C200",
    "Portland-Metro": "002%2C003%2C005%2C032%2C033%2C037%2C051%2C194",
    "Prineville": "048",
    "Redmond": "011",
    "Roseburg": "017",
    "Salem": "007",
    "Santiam Canyon": "049",
    "St Helens": "006",
    "The Dalles": "020",
    "Tillamook": "013",
    "Tualatin": "051",
    "Valley and Southern Oregon": "007%2C008%2C016%2C017%2C018%2C021%2C038%2C039%2C040%2C045%2C049%2C050%2C052",
    "Vancouver,WA.": "194",
    "Woodburn": "052",
    "Out of State": "199%2C195%2C194%2C197%2C196",
    "California": "196",
    "Idaho": "197",
    "Washington": "195",
    "Other States": "199"
}


class OregonJob(Job):
    """This class stores data extracted by the OregonJobs Scraper."""

    def __init__(self, title):
        super().__init__()
        self.title = title
        self.deadline = ''

    def as_csv(self) -> str:
        posted = f"{self.date.month}/{self.date.day}/{self.date.year}"
        return f'"{self.title}",{self.deadline},{self.location},{posted},"{self.salary}",{self.score},{self.url}'

    def csv_header(self) -> str:
        return "Title,Deadline,Location,Date Posted,Salary,Score,URL"


class OregonJobs(Scraper):
    """This class scrapes listings from the Oregon Employment Department's jobs page."""
    domain = "secure.emp.state.or.us"

    @property
    def jobs_per_page(self) -> int:
        return 20

    def __init__(self, config: SectionProxy, quiet: bool = False, stop=None):
        """Instantiate an OregonJobs Scraper.
        :param config: Configuration details (must include keyword and user_agent)
        :param quiet: When True (default: False), Scraper messages are suppressed.
        :param stop: When >= 1 (default: None), ends scraping once {stop} jobs are returned.
        """
        super().__init__(
            config["keyword"], r'<TD class="text_jobdetail">(?P<id>\d+)', config["user_agent"], quiet, stop
        )
        loc = "Oregon"
        if config["where"] in str_to_loc:
            loc = str_to_loc[config["where"]]
        self.base = f"https://{self.domain}/jobs/index.cfm?"
        self.query = f"{self.base}keyword={self.job_kw}&keytype=Both&loc={loc}&"
        self.query += "calling_pg=search_home&location_content=joblist.cfm&showcounts=Y&system=new&type=N&lang=E&start="

    def __next__(self):
        self.start_next()
        if self.x % self.jobs_per_page == 1:
            try:
                html = self.url_open_read_decode(self.query + str(self.x))
                if self.x == 1:
                    self.init_results(html, r"\s+There\s+are <b>(?P<hits>\d+)")
                self.turn_page(html)
            except Exception:
                raise StopIteration
        jid = self.next_id_match().group("id")
        url = f"{self.base}location_content=jobdisplay.cfm&ord={jid}&system=new&type=N&lang=E"
        if jid in self.ids:
            self.print(f"Skipping duplicate ({url})")
            return self.__next__()
        else:
            self.ids.append(jid)
            try:
                page = BeautifulSoup(self.url_open_read_decode(url), "html.parser")
                tds = page.find_all("td")
                job = OregonJob(tds[13].text[1:-1])
                spans = page.find_all("span")
                job.deadline = spans[8].text
                if spans[9].text not in ["Or", "United St"]:
                    job.location = spans[9].text
                tokens = spans[10].text.split('/')
                job.date = datetime(int(tokens[2]), int(tokens[0]), int(tokens[1]))
                job.url = url
                job.description = tds[29].text
                match = re.search(r"(?P<rate>\$[0-9,.]{5,})\s+", spans[16].text)
                if match:
                    text = match.group("rate")
                    match = re.search(r"(?P<rates>\$[0-9,.]{5,}\s+to\s+\$[0-9,.]{5,})", spans[16].text)
                    if match:
                        text = match.group("rates")
                    text += " per " if "\tper" in spans[16].text else ''
                    for unit in ["Hour", "Day", "Week", "Month", "Year"]:
                        text += unit if unit in spans[16].text else ''
                    job.salary = text
                return job
            except HTTPError:
                return self.__next__()

    @classmethod
    def describe(cls) -> str:
        return f"OregonJobs ({cls.domain})"
