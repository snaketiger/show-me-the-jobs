#
# Copyright 2023 Patrick S. Overton and Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from setuptools import setup

setup(
    name='show-me-the-jobs',
    version='0.1.0',
    packages=['jobscraper'],
    url='https://gitlab.com/py-pals/show-me-the-jobs/',
    license='Apache License 2.0',
    author='Patrick S. Overton',
    author_email='pso-eng@proton.me',
    description='This job scraping tool is designed to provide extensible support for multiple websites.',
    keywords='job scraper tool',

    classifiers=[
        'Private :: Do Not Upload',
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Internet',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: Python :: 3.12'
    ],

    python_requires='>=3.8',
    install_requires=['beautifulsoup4'],
    entry_points={
        'console_scripts': [
            'show-me-the-jobs=show-me-the-jobs:main'
        ]
    }
)
